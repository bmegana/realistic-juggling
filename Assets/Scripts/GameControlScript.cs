﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControlScript : MonoBehaviour {

    public static GameControlScript instance;

    public bool failed = false;
    public bool passed = false;

    public int levelOneNumberCrossThrows = 1;
    public int levelTwoNumberCrossThrows = 2;
    public int levelThreeNumberCrossThrows = 10;
    public int levelFourNumberCrossThrows = 14;
    public int levelFiveNumberCrossThrows = 18;

    public int currentNumCrossThrows = 0;

    public Text throwsLeftText;

    private string currentSceneName;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
        else if (instance != null) {
            Destroy(this);
        }
    }

    void Start() {
        currentSceneName = SceneManager.GetActiveScene().name;
        Debug.Log("Current Scene: " + currentSceneName);
    }

    private void Update() {
        if (currentSceneName.Equals("Main Menu")) {
            throwsLeftText.text = "";
        }

        if (currentSceneName.Equals("Level1")) {
            throwsLeftText.text = "Throws Left: " +
                (levelOneNumberCrossThrows - currentNumCrossThrows);
            if (currentNumCrossThrows >= levelOneNumberCrossThrows) {
                passed = true;
                MenuControllerScript.instance.levelToGoNext = "Level2";
            }
        }
        else if (currentSceneName.Equals("Level2")) {
            throwsLeftText.text = "Throws Left: " +
                (levelTwoNumberCrossThrows - currentNumCrossThrows);
            if (currentNumCrossThrows >= levelTwoNumberCrossThrows) {
                passed = true;
                MenuControllerScript.instance.levelToGoNext = "Level3";
            }
        }
        else if (currentSceneName.Equals("Level3")) {
            throwsLeftText.text = "Throws Left: " +
                (levelThreeNumberCrossThrows - currentNumCrossThrows);
            if (currentNumCrossThrows >= levelThreeNumberCrossThrows) {
                passed = true;
                MenuControllerScript.instance.levelToGoNext = "Level4";
            }
        }
        else if (currentSceneName.Equals("Level4")) {
            throwsLeftText.text = "Throws Left: " +
                (levelFourNumberCrossThrows - currentNumCrossThrows);
            if (currentNumCrossThrows >= levelFourNumberCrossThrows) {
                passed = true;
                MenuControllerScript.instance.levelToGoNext = "Level5";
            }
        }
        else if (currentSceneName.Equals("Level5")) {
            throwsLeftText.text = "Throws Left: " +
                (levelFiveNumberCrossThrows - currentNumCrossThrows);
            if (currentNumCrossThrows >= levelFiveNumberCrossThrows) {
                passed = true;
                MenuControllerScript.instance.levelToGoNext = "You Win";
            }
        }

        if (passed) {
            MenuControllerScript.instance.ActivateYouPassed();
        }
    }

    public void PrintNumCrossThrows() {
        Debug.Log("Current Number of Cross Throws: " + currentNumCrossThrows);
    }
}
