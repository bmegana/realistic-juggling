﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallDropTimer : MonoBehaviour {

    private Rigidbody2D ballRb2D;

    public float timeToDrop = 5f;
    private float time = 0f;

	void Start () {
        ballRb2D = GetComponent<Rigidbody2D>();
    }
	
	void Update () {
        time += Time.deltaTime;
        if (time < timeToDrop) {
            ballRb2D.velocity = Vector2.zero;
        }
	}
}
