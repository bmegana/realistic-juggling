﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftHandJugglingScript : MonoBehaviour {

    public GameObject leftHand;
    public float movementMultiplier = 5f;
    public float handMoveDistance = 1.5f;
    public float lerpTime = 1f;

    public GameObject ball;
    public float ballThrowSpeed = 10f;
    private Rigidbody2D ballOneRigidbody2D;

    public bool catching = false;
    public bool throwing = false;

    private bool throwUpPressedDown = false;
    private bool throwAcrossPressedDown = false;
    private bool isRetracting = false;

    private float startTime;
    private float percentage = 0f;

    private Vector2 startVec;
    private Vector2 endVec;
    
    private void CheckThrowingUp() {
        if (Input.GetButtonDown("Left Hand Throw Up") && 
            !throwUpPressedDown && !isRetracting) {
            if (!throwUpPressedDown) {
                throwUpPressedDown = true;
                startTime = Time.time;

                startVec = this.transform.position;
                endVec = this.transform.position +
                    (Vector3.up * handMoveDistance);
            }
        }

        if (throwUpPressedDown || isRetracting) {
            MoveHandThrowUp();
        }

        if (percentage >= 1.0f) {
            percentage = 1.0f;
            if (throwUpPressedDown) {
                throwUpPressedDown = false;
                isRetracting = true;

                ThrowBallUp();

                endVec = startVec;
                startVec = this.transform.position;

                startTime = Time.time;
            }
            else {
                isRetracting = false;
                this.transform.position = endVec;
            }
        }

        if (catching && Input.GetButtonUp("Left Hand Throw Up")) {
            throwing = true;
            ThrowBallUp();
        }
    }

    private void MoveHandThrowUp() {
        float timeElasped = Time.time - startTime;
        if (isRetracting) {
            percentage = (timeElasped * movementMultiplier) / lerpTime;
        }
        else {
            percentage = (timeElasped * timeElasped * movementMultiplier)
                / lerpTime;
        }

        float x = startVec.x;
        float y = Mathf.Lerp(startVec.y, endVec.y, percentage);
        this.transform.position = new Vector2(x, y);
    }

    private void ThrowBallUp() {
        if (ball != null) {
            catching = false;
            throwing = true;

            ballOneRigidbody2D = ball.GetComponent<Rigidbody2D>();

            Physics2D.IgnoreCollision(
                gameObject.GetComponent<Collider2D>(),
                ball.GetComponent<Collider2D>(), true);

            ballOneRigidbody2D.AddForce(
                Vector2.up * ballThrowSpeed * percentage,
                ForceMode2D.Impulse
            );
        }
    }

    private void CheckThrowingAcross() {
        if (catching && Input.GetButtonDown("Left Hand Throw Across") &&
            !throwAcrossPressedDown && !isRetracting) {
            if (!throwUpPressedDown) {
                throwAcrossPressedDown = true;
                startTime = Time.time;

                startVec = this.transform.position;
                endVec = new Vector2(
                    this.transform.position.x + handMoveDistance,
                    this.transform.position.y + handMoveDistance
                );
            }
        }

        if (throwAcrossPressedDown || isRetracting) {
            MoveHandThrowAcross();
        }

        if (percentage >= 1.0f) {
            percentage = 1.0f;
            if (throwAcrossPressedDown) {
                throwAcrossPressedDown = false;
                isRetracting = true;

                ThrowBallAcross();

                endVec = startVec;
                startVec = this.transform.position;

                startTime = Time.time;
            }
            else {
                isRetracting = false;
                this.transform.position = endVec;
            }
        }

        if (catching && Input.GetButtonUp("Left Hand Throw Across")) {
            ThrowBallAcross();
        }
    }

    private void MoveHandThrowAcross() {
        float timeElasped = Time.time - startTime;
        if (isRetracting) {
            percentage = (timeElasped * movementMultiplier + 1f) / lerpTime;
        }
        else {
            percentage = (timeElasped * timeElasped * movementMultiplier)
                / lerpTime;
        }

        float x = Mathf.Lerp(startVec.x, endVec.x, percentage);
        float y = Mathf.Lerp(startVec.y, endVec.y, percentage);
        this.transform.position = new Vector2(x, y);
    }

    private void ThrowBallAcross() {
        if (ball != null) {
            catching = false;
            throwing = true;

            ballOneRigidbody2D = ball.GetComponent<Rigidbody2D>();
            ball.GetComponent<BallInfo>().wasThrownAcross = true;

            Physics2D.IgnoreCollision(
                gameObject.GetComponent<Collider2D>(),
                ball.GetComponent<Collider2D>(), true);

            ballOneRigidbody2D.AddForce(
                new Vector2(0.5f, 1) * ballThrowSpeed * percentage * 0.95f,
                ForceMode2D.Impulse
            );
        }
    }

    private void CatchBall(GameObject ball) {
        if (this.ball == null) {
            this.ball = ball;
        }
        BallInfo ballInfo = this.ball.GetComponent<BallInfo>();
        if (ballInfo.wasThrownAcross) {
            GameControlScript.instance.currentNumCrossThrows++;
            GameControlScript.instance.PrintNumCrossThrows();
            ballInfo.wasThrownAcross = false;
        }
        ballOneRigidbody2D = this.ball.GetComponent<Rigidbody2D>();
    }

    private void HoldOntoBall() {
        ballOneRigidbody2D.velocity = Vector2.zero;
        this.ball.transform.position = gameObject.transform.position;
    }

    void Update() {
        if (catching && this.ball != null) {
            HoldOntoBall();
        }

        if (Input.GetButtonDown("Left Hand Button")) {
            catching = true;
        }
        else if (this.ball == null) {
            catching = false;
        }

        CheckThrowingUp();
        CheckThrowingAcross();

        if (Input.GetButtonUp("Left Hand Button")) {
            catching = false;
        }
    }

    void OnTriggerEnter2D(Collider2D collision) {
        GameObject ball = null;
        if (collision.gameObject.CompareTag("Ball")) {
            ball = collision.gameObject;
            if (catching && !throwing) {
                CatchBall(ball);
            }
        }
    }

    void OnTriggerStay2D(Collider2D collision) {
        GameObject ball = null;
        if (collision.gameObject.CompareTag("Ball")) {
            ball = collision.gameObject;
            if (catching && !throwing) {
                CatchBall(ball);
            }
        }
    }

    void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Ball")) {
            throwing = false;

            if (this.ball != null) {
                Physics2D.IgnoreCollision(
                    gameObject.GetComponent<Collider2D>(),
                    ball.GetComponent<Collider2D>(), false);
                this.ball = null;
            }
        }
    }
}
