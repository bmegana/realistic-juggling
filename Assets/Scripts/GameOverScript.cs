﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour {

    void Start() {
        Time.timeScale = 1.0f;
    }

    void Update() {
        if (GameControlScript.instance.failed &&
            Input.GetKeyDown(KeyCode.R)) {
            GameControlScript.instance.failed = false;
            string currentSceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(currentSceneName);
        }
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Ball")) {
            GameControlScript.instance.failed = true;
            MenuControllerScript.instance.ActivateGameOverMenu();
            Time.timeScale = 0;
        }
    }
}
