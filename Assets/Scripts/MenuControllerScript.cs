﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MenuControllerScript : MonoBehaviour {

    public static MenuControllerScript instance;

    public EventSystem eventSystem;

    public GameObject mainMenuPanel;
    public GameObject gameOverPanel;
    public GameObject controlsPanel;
    public GameObject optionsPanel;
    public GameObject pausePanel;
    public GameObject youPassedPanel;
    public GameObject youWinPanel;
    public string levelToGoNext;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
        else if (instance != null) {
            Destroy(this);
        }
    }

    private void Start() {
        DeactivateAllPanels();
        if (!IsPlayingGame()) {
            ActivateMainMenu();
        }
    }

    private void Update() {
        if (IsPlayingGame() && !GameControlScript.instance.failed) {
            if (Input.GetButtonDown("Pause")) {
                if (Time.timeScale == 1) {
                    ActivatePauseMenu();
                }
                else {
                    DeactivatePauseMenu();
                }
            }
        }

        if (GameControlScript.instance.passed) {
            Time.timeScale = 0;
            if (levelToGoNext.Equals("You Win")) {
                ActivateYouWin();
            }
            else {
                ActivateYouPassed();
            }

            if (Input.GetKey(KeyCode.A) && Input.GetKeyDown(KeyCode.L)) {
                GameControlScript.instance.currentNumCrossThrows = 0;
                Time.timeScale = 1;
                if (levelToGoNext.Equals("You Win")) {
                    QuitGame();
                }
                else {
                    LevelLoader(levelToGoNext);
                }
            }
        }
    }

    public void QuitGame() {
        Application.Quit();
    }

    public void LevelLoader(string levelName) {
        DeactivateAllPanels();
        SceneManager.LoadScene(levelName);
    }

    public void LoadMainMenuScene() {
        Time.timeScale = 1;
        DeactivateAllPanels();        
        SceneManager.LoadScene("MainMenu");
    }

    public void ChangePanel(string panelName) {
        DeactivateAllPanels();
        if (panelName.Equals("MainMenu")) {
            ActivateMainMenu();
        }
        else if (panelName.Equals("Controls")) {
            controlsPanel.SetActive(true);
            AutoSelectButton(controlsPanel, "BackButton");
        }
        else if (panelName.Equals("Options")) {
            optionsPanel.SetActive(true);
            AutoSelectButton(optionsPanel, "BackButton");
        }
        else if (panelName.Equals("Game Over")) {
            gameOverPanel.SetActive(true);
            AutoSelectButton(gameOverPanel, "Continue");
        }
        else if (panelName.Equals("You Passed")) {
            ActivateYouPassed();
        }
    }

    public void ActivateMainMenu() {
        mainMenuPanel.SetActive(true);
        AutoSelectButton(mainMenuPanel, "StartGameButton");
    }

    public void ActivatePauseMenu() {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
        AutoSelectButton(pausePanel, "ContinueButton");
    }

    public void DeactivatePauseMenu() {
        pausePanel.SetActive(false);
        Time.timeScale = 1;
    }

    public void ActivateYouPassed() {
        youPassedPanel.SetActive(true);
    }

    public void ActivateYouWin() {
        DeactivateAllPanels();
        youWinPanel.SetActive(true);
    }

    public void ActivateGameOverMenu() {
        gameOverPanel.SetActive(true);
    }

    public void DeactivateAllPanels() {
        mainMenuPanel.SetActive(false);
        controlsPanel.SetActive(false);
        optionsPanel.SetActive(false);
        pausePanel.SetActive(false);
        gameOverPanel.SetActive(false);
        youPassedPanel.SetActive(false);
        youWinPanel.SetActive(false);
    }

    public void BackToPreviousPanel() {
        DeactivateAllPanels();
        if (!IsPlayingGame()) {
            ActivateMainMenu();
        }
        else {
            ActivatePauseMenu();
        }
    }

    private void AutoSelectButton(GameObject panel, string buttonName) {
        GameObject buttonObject = panel.transform.Find(buttonName).gameObject;
        if (buttonObject != null) {
            Button button = buttonObject.GetComponent<Button>();

            button.OnSelect(null); //highlights the selected button
            eventSystem.SetSelectedGameObject(buttonObject);
        }
    }

    private bool IsPlayingGame() {
        return !(SceneManager.GetActiveScene().name.Equals("Level0") ||
            SceneManager.GetActiveScene().name.Equals("MainMenu"));
    }
}
